package services

import java.text.SimpleDateFormat
import java.time.LocalDateTime
import java.util.Date

import akka.actor.ActorSystem
import akka.http.scaladsl.model.DateTime
import javax.inject._
import play.api.inject.ApplicationLifecycle
import play.api.libs.functional.syntax.{unlift, _}
import play.api.libs.json._
import play.api.libs.ws.WSClient

import scala.concurrent.duration.FiniteDuration
import scala.concurrent.duration._
import scala.concurrent.ExecutionContext.Implicits.global

/**
 * This class demonstrates how to run code when the
 * application starts and stops. It starts a timer when the
 * application starts. When the application stops it prints out how
 * long the application was running for.
 *
 * This class is registered for Guice dependency injection in the
 * [[play.api.inject.Module]] class. We want the class to start when the application
 * starts, so it is registered as an "eager singleton". See the code
 * in the [[play.api.inject.Module]] class to see how this happens.
 *
 * This class needs to run code when the server stops. It uses the
 * application's [[ApplicationLifecycle]] to register a stop hook.
 */
@Singleton
class VergeSenseTask @Inject()(ws: WSClient , actorSystem: ActorSystem) {

  // This code is called when the application starts.
  private var started: Boolean = false

  case class IOfficeSensor(utilized: Boolean, count: Int, startDate: String, sensorUid: String, endDate: String)
  implicit val iOfficeSensorWrites: Writes[IOfficeSensor] = (
    (JsPath \ "utilized").write[Boolean] and
      (JsPath \ "count").write[Int] and
      (JsPath \ "startDate").write[String] and
      (JsPath \ "sensorUid").write[String] and
      (JsPath \ "endDate").write[String]
    ) (unlift(IOfficeSensor.unapply))

  case class VergeSensor(id: String, count: Int, timestamp: String)
  implicit val vergeSensorReads: Reads[VergeSensor] = (
    (JsPath \ "id").read[String] and
      (JsPath \ "count").read[Int] and
      (JsPath \ "timestamp").read[String]
    ) (VergeSensor.apply _)

  def startVergeSenseTask(delayTime: FiniteDuration, apiKey: String) = {

    if(!started) {
      started = true;

      actorSystem.scheduler.schedule(1.second, delayTime) {
        // No Auth call. Just add key as x-api-key in http header
        // Step 1: get sensor lists
        ws.url("https://api.vergesense.com/sensors/state")
          .addHttpHeaders("x-api-key" -> apiKey)
          .get().map(response => {

          processVergeSenseResponse(response.body)
        }
        )
      }
    }
  }

  private def processVergeSenseResponse(jsonResponse: String): Unit ={
    val responseJsonValue: JsValue = Json.parse(jsonResponse);

    val vergeSensorResult: JsResult[List[VergeSensor]] = (responseJsonValue \ "sensors").validate[List[VergeSensor]]
    val sensorList: List[VergeSensor] = vergeSensorResult.get

    val format = "yyyy-MM-dd'T'hh:mm:ssZ"
    val formatter = new SimpleDateFormat(format)

    for (sensor <- sensorList) {
      // for each verge sensor, create iOffice sensor object
      val utilized: Boolean = sensor.count > 0
      val officeSensor: IOfficeSensor = new IOfficeSensor(utilized, sensor.count, sensor.timestamp, sensor.id.toString, formatter.format(DateTime.now))
      postToIOffice(officeSensor)
    }
  }

  private def postToIOffice(officeSensor: IOfficeSensor): Unit ={
    val sensorJson = Json.toJson(officeSensor)

    ws.url("https://hq.iofficeconnect.com/api/v3/sensors/utilization-data").post(sensorJson).map(response => {

      val responseBody = response.body
      println(responseBody)
    }
    )
  }

}
